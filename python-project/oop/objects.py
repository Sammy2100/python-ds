# objects

class Rational(object):
    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator

    def __repr__(self):
        return f"{self.numerator}/{self.denominator}"

    def __mul__(self, number):
        if isinstance(number, (int, Rational)):
            return Rational(self.numerator * number.numerator, self.denominator * number.denominator)
        else:
            raise TypeError(f"Expected number to be int or Rational. Got is {type(number)}")

    def __rmul__(self, number):
        return self.__mul__(number)

    def _gcd(self):
        smaller = min(self.numerator, self.denominator)
        small_divisors = {i for i in range(1, smaller + 1) if smaller % i == 0}
        larger = max(self.numerator, self.denominator)
        common_divisors = {i for i in small_divisors if larger % i == 0}
        return max(common_divisors)

    def reduce(self):
        gcd = self._gcd()
        self.numerator = self.numerator / gcd
        self.denominator = self.denominator / gcd
        return self


fraction = Rational(2, 3)
fraction.handler = 3
print(fraction)


class Person:
    def __init__(self, surname, firstname):
        self.surname = surname
        self.firstname = firstname
        self.fullname = self._getFullname()

    def __repr__(self):  # double underscore (dunder) methods
        return self.fullname

    def _getFullname(self): # any function that starts with an underscore is a helper (private) function
        return f'{self.surname} {self.firstname}'

    def getfullname(self):
        return self._getFullname()


# sam = Person('Iyomere', 'Samuel')
# print(sam.fullname)
# print(sam.getfullname())
# print(sam)

# inheritance

class Rectangle(object):
    def __init__(self, height, length):
        self.height = height
        self.length = length

    def ares(self):
        return self.height * self.length

    def perimeter(self):
        return 2 * (self.height + self.length)


class Square(Rectangle):
    def __init__(self, length):
        super(Square, self).__init__(length, length)