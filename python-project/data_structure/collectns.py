# Collections

from collections import namedtuple, deque, Counter, defaultdict
import math
import time
# import timing

# print(math.sqrt(4))


Vector3 = namedtuple('Vector', ['x', 'y', 'z'])

vec = Vector3(1, 2, 3)
# print(dir(vec))
# print(vec.x)


def tfunc(a, b, c):
    print(a, b, c)
# tfunc(*vec)


# DEQUE
my_list = [2, 3, 4, 5]


def deque_list(items):
    d = deque(items)
    print(d)
    d.append(10)
    print(d)
    d.appendleft(20)
    print(d)
# deque_list(my_list)


def list_impl():

    l = list()
    for i in range(400000):  l.insert(0, i)
    # timing.log('list implementation')


def deque_impl():
    d = deque()
    for i in range(400000):  d.appendleft(i)
    # timing.log('deque implementation')
# list_impl()
# deque_impl()


# Counter
ele = ['a', 'b', 'a', 'c', 'b', 'b', 'd']
c = Counter(ele)
# print(c)
# print(c.most_common(1))
# print(c['b'])
# print(c['z'])


# The Counter is equivalent to the function below.
def count(items):
    count_dict = {}
    for item in items:
        if item in count_dict:
            count_dict[item] += 1
        else:
            count_dict[item] = 1
    return count_dict
# print(count(ele))


# Default Dict

# The Default Dict is equivalent to the function below.
def count_default(items):
    count_dict = defaultdict(int)
    for item in items:
        count_dict[item] +=1
    return count_dict
print(count_default(ele))