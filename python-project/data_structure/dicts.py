# DICTIONARY (DICT)

me_dict = {'name': "Dylan", 'age': 28, 'height': 157.5, 'weight': 56.5, 'hair': 'brown', 'eyes': 'brown'}

# print(me_dict)
# print(f"My name is {me_dict['name']}")
# print(f"I have {me_dict['hair']} hair")

key_list = ['name', 'age', 'height', 'weight', 'hair', 'eyes']
value_list = ['Dylan', 28, 167.5, 56.5, 'brown', 'brown', True]


def zipping(keys, values):
    print(keys)
    print(values)
    list_key_value_pair = list(zip(keys, values))
    print(list_key_value_pair)
    dict_key_value_pair = dict(zip(keys, values))
    print(dict_key_value_pair)


# zipping(key_list, value_list)


def add_update_dict(items):
    print(items)
    # add
    items['favourite book'] = 'The little prince'
    # update
    items['hair'] = 'black'
    items.update({'favourite color': 'orange', 'siblings': 1})
    print(items)


# add_to_dict(me_dict)


def delete_pop(items):
    print(items)
    # delete
    del items['favourite color']
    items.pop(['siblings'])
    print(items)


# update_dict(me_dict)


def print_dict(dict_items):
    print(dict_items.keys())
    print(dict_items.values())
    print(dict_items.items())


# print_dict(me_dict)


def searching_dict(item):
    if 'siblings' in item:
        print(item['sibling'])
    else:
        print(None)
    if 'brown' in item.values():
        print('item found')
    else:
        print('item not found')


# searching_dict(me_dict)


def searching_by_get_dict(items):
    print(items.get('eyes'))
    print(items.get('nose'))
    print(items.get('ear', False))
# searching_by_get_dict(me_dict)


def sort_dict(dicts):
    print(sorted(map(str, dicts.keys()))) # sort by keys
    print(sorted(map(str, dicts.values()))) # sort by values
    print(sorted(dicts.items())) # sort key_value by keys
    print(sorted(map(lambda kv: (kv[0], str(kv[1])), dicts.items()), key=lambda kv: kv[1])) # sort key_value by values
# sort_dict(me_dict)


def iterate_dict(dicts):
    # keys
    for k in dicts: print(k)
    # values
    for v in dicts.values(): print(v)
    # key_value
    for k, v in dicts.items():
        print(f'{k}: {v}')
# iterate_dict(me_dict)


def comprehension_dict(dicts):
    square_lut = {x: x**2 for x in range(10)}
    print(square_lut)
    items = {k: f'{type(v)} {v}' for k, v in dicts.items()}
    print(items)
comprehension_dict(me_dict)