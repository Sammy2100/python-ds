def data_types():
    print(type('Sam'))
    print(type(10))
    print(type(10.2))
    print(type(True))
    print(type([1, 2, 3]))
    print(type((1, 2, 3)))
    print(type({1, 2, }))


# LISTS
list_of_lists = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
mixed_list = [26, False, 'some words', 1.264]  # - heterogeneous list

grocery_list = ['chicken', 'onions', 'rice', 'peppers', 'bananas']


# print(grocery_list[2])

# sub list of slice of a list
def slice_list(items):
    print(items)
    print(items[1:4])  # include from index 1 to the item before index 4
    print(items[3:])  # starts from index 3 to the last item in the list
    print(items[:3])  # starts from the first item in the list to the item before index 3
    print(items[-1])  # retrieves the item in the last index of the list
    print(items[-3:])  # starts from the last item in the list and retrieves 3 items
    # adding step size
    print(items[::2])  # starts from the beginning to the end using a step size of 2.
    print(items[4:1:-1])  # starts from the index 4 to the item before index 1 in reverse order.


# looping through a list using the index and range
def loop_list_index(items):
    for i in range(0, len(items), 2):
        print(i, items[i])


# loop_list_index(grocery_list)
def update_list_items(items):
    print('before update: ', items)
    # mutate (modify/change state of) items in the list
    items[-1] = 'oranges'
    items[1:3] = ['carrots', 'couscous']
    print('after update: ', items)


# update_list_items(grocery_list);
def add_list_items(items):
    print('before add: ', items)
    items.append('squash')  # using append to add a single item
    items.extend(['bread', 'salt'])  # using extend to add a list of items
    print('after add: ', items)


# add_list_items(grocery_list)
def delete_list_items(items):
    print('before delete: ', items)
    del items[-1]  # delete the last item in the list by index
    items.pop()  # delete the last item in the list using POP
    items.pop(2)  # delete an item in the list using POP at Index
    print('after delete: ', items)


# delete_list_items(grocery_list)
def sort_list(items):
    print('before sort: ', items)
    items.sort()
    # sorted(items)
    print('asc sort: ', items)
    items.sort(reverse=True)
    # sorted(items, reverse=True)
    print('desc sort: ', items)


# sort_list(grocery_list)
def list_exercise():
    my_list = list(range(10))
    print(my_list)
    print(my_list[-2:])  # prints the last 2 items in the list
    print(my_list[0::2])  # print every other items in the list starting from index 0 to the last item
    print(my_list[1::2])  # print every other items in the list starting from index 1 to the last item


list_exercise()
