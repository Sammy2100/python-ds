# TUPLES

example_tuple = ('Dylan', 26, 167.6, True)


# print(example_tuple)

def first_last(s):
    return s[0], s[3], s[-1]


# print(first_last('Hello!'))

def unpacking_tuples(t):
    name, age, height, has_dog = t
    print(name)
    print(age)
    print(height)
    print(has_dog)


unpacking_tuples(example_tuple)
