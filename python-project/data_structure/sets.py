# SETS

example_set = {'Dylan', 26, 167.6, True}


def delete_from_set(items):
    items.pop()
    print(items)


def add_to_set(items):
    items.add('True')
    items.update([58.1, 'brown'])
    print(items)


# add_to_set(example_set)

list_of_numbers = [23, 609, 348, 10, 5, 23, 340, 82]
tuple_of_letters = ('a', 'b', 'q', 'c', 'c', 'd', 'r', 'a')


# print(set(list_of_numbers))
# print(set(tuple_of_letters))

def set_of_two_lists(items_a, items_b):
    print(items_a.intersection(items_b))
    print(items_a.union(items_b))
    print(items_a.difference(items_b))
    print(items_b.difference(items_a))
    print(items_a.symmetric_difference(items_b))


student_a_courses = {'history', 'english', 'biology', 'theatre'}
student_b_courses = {'biology', 'english', 'mathematics', 'computer science'}
set_of_two_lists(student_a_courses, student_b_courses)
