# Big O Notation - Space/Memory Complexity

from collections import defaultdict
import time


def time_func(func, *args):
    ts = time.time()
    func(*args)
    elapsed_time = time.time() - ts
    print(str(func) + ' elapsed time :', elapsed_time)
    return elapsed_time


# using only recursion
def fibonacci_recursive(n):
    if n == 0: return 0
    if n == 1:
        return 1
    else:
        return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)


# counting fibonacci sequence execution without using memory
def fibonacci_count(n, d):
    d[n] += 1
    if n == 0: return 0, d
    if n == 1:
        return 1, d
    else:
        n1, _ = fibonacci_count(n - 1, d)
        n2, _ = fibonacci_count(n - 2, d)
        return n1 + n2, d


# N = 25
# ans, d = fibonacci_count(N, defaultdict(int))
# for i in range(N):
#     print(i, d[i])


# counting fibonacci sequence execution faster using memory (memoization)
def fibonacci_mem(n, d):
    if n in d:
        return d[n]
    elif n == 0:
        ans = 0
    elif n == 1:
        ans = 1
    else:
        ans = fibonacci_mem(n - 1, d) + fibonacci_mem(n - 2, d)
    d[n] = ans
    return ans


# n = 35
# time_func(fibonacci_recursive, n)
# time_func(fibonacci_mem, n, {0:0, 1:1})


# factorial using iteration
def factorial_iteration(n):
    ans = 1
    for i in range(n, 1, -1):
        ans *= i
    return ans


# factorial using recursion
def factorial_recursive(n):
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial_recursive(n - 1)


def factorial_inbuilt(n):
    import math
    return math.factorial(n)


n = 1000
# time_func(factorial_inbuilt, n)
time_func(factorial_iteration, n)
time_func(factorial_recursive, n)


# [0, 1, 2, ..., N] => O(N) size
# [[0, 1, 2, ..., N], [0, 1, 2, ..., N], ..., N] => O(N^2) size
# {0: 0, 1: 1, 2: 2, ..., N: N} => O(N) size
# {(0, 'a'): 0, (0, 'b'): 1, (1, 'b'): 3} => O(N^2) size


# using more space
def sum_list(n):
    numbers = list(range(n))
    return sum(numbers)


# using less space
def sum_iter(n):
    number = 0
    sum = 0
    while number < n:
        sum += number
        number += 1
    return sum


# insertion using a binary search method
def binary_insert(n, d):
    print(d)
    length = len(d)
    if length == 0 or length == 1:
        print('append')
        d.append(n)
    else:
        mid_point = length // 2
        if d[mid_point] < n:
            print('recurse')
            binary_insert(n, d[mid_point:])
        else:
            d.append(n)
    return d

values = [1, 2, 3, 4, 6, 7]
# print(binary_insert(5, values))
