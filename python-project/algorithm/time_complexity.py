# Big O Notation - Time/Computational Complexity
import random
import time


# function to time the running time of a function
# based on its input
def time_func(func, *args):
    ts = time.time()
    func(*args)
    elapsed_time = time.time() - ts
    print(elapsed_time)
    return elapsed_time


# O(N) - Linear Algorithm - Order N
# O(N) => O(N + 1) => (2N)


def sum_n(n):
    sum_ = 0
    for n in range(n + 1):
        sum_ += n
    return sum_


# print(sum_n(5))


# O(1) - Constant Algorithm - Order 1
# The GAUSS SUM
def sum_gauss(n):
    sum_ = n * (n + 1) // 2
    return sum_


# print(sum_gauss(5))
test_val = 1
assert sum_n(test_val) == sum_gauss(test_val)


def random_list(n, sort=False):
    list_ = [random.randint(0, 10 * n) for _ in range(n)]
    return sorted(list_) if sort else list_


def find_ele(list_, ele):
    for i in list_:
        if i == ele:
            return True
    return False


my_list = random_list(5)
time_func(find_ele, my_list, 5)


# If list is sorted, consider using O(logN)
# Note that sorting takes N(LogN) time
def find_ele(list_, ele):
    for i in list_:
        if i == ele:
            return True
        if i > ele:
            # stop early. No need to iterate to the end of the list
            return False
    return False


# O(Log2N) => O(LogN) Log base 2 of N
# For a better O(logN) that does divide and conquer using recursion
def find_ele_binary(items, ele):
    length = len(items)
    if length < 1: return False
    mid_point = length / 2
    if items[mid_point] == ele:
        return True
    elif items[mid_point] > ele:
        # select only the first half of the list to search
        return find_ele_binary(items[:mid_point], ele)
    else:
        # select only the second half of the list to search
        return find_ele_binary(items[mid_point + 1:], ele)


# Order N square O(N^2) time complexity
def find_in_string(string):
    for i, char in enumerate(string): # n step
        for char in string[i:]: # roughly size n each step in above loop
            pass

