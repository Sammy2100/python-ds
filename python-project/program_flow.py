# Program Flow

def cube(number):
    return number ** 2


# print(cube(2))

def string_manipulation(my_name, my_age):
    # my_name = "Samuel"
    # my_age = 33
    print('My name is', my_name)
    print('My age is', my_age)
    print('In 10 years time, I will be', my_age + 10)
    print("In 10 years time, i will be {} years old.".format(my_age + 10))


my_list = [1, 2, 3, 34, 21, 'Samuel', 'Iyomere']
my_tuple = (1, '2', True, 234)
my_set = {1, 4, 2, 'Sam', True}


def display(items):
    for item in items:
        print(item)


# display(my_list)

def range_example():
    for i in range(0, 100, 1):
        print(i)


# range_example()

def is_prime(number):
    if number < 2:
        return False
    for factor in range(2, number):
        if number % factor == 0:
            return False
        return True


def print_primes(n):
    for number in range(1, n):
        if is_prime(number):
            print('%d is a prime number' % number)


# print_primes(42);
list_of_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
list_comprehension = [number for number in list_of_numbers if is_prime(number)]


# print(list_comprehension)

def print_todo_args(*args):
    print('i need to: ')
    for arg in args:
        print(' ' + arg)

# print_todo_args('watch tv', "read", 'eat', 'sleep')

def object_attribute(obj):
    [print(attr) for attr in dir(obj)]
    # print(dir(obj))

object_attribute(list_of_numbers)